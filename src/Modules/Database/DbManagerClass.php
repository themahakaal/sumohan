<?php

/**
 * @author Sukhdev Mohan <sukhdev.mohan@gmail.com>
 * @version
 */

namespace Sumohan\Modules\Database;

require_once __DIR__ . "/../../../vendor/autoload.php";

use Sumohan\Config\Configuration as config;
use Sumohan\Modules\Database\Database as db;

class DBM
{
    private static $dblink = null;

    public function __construct()
    {
        self::$dblink = new db();
        self::$dblink->setInstance(config::getDSN(), config::getDbUsername(), config::getDbPassword());
    }

    public static function getDblink()
    {
        return self::$dblink;
    }

    public static function setOffDblink()
    {
        self::$dblink->__destruct();
        self::$dblink = null;
    }
}