<?php

namespace Sumohan\Modules\Database;

require_once __DIR__ . "/../../../vendor/autoload.php";

use Sumohan\Config\Configuration as c;
/**
 * Description of Database
 *
 * @author sumohan
 */
class Database{
    
    protected static $db = null;

    /**
     * Constructor
     */
    public function __construct() {
        if(null !== self::$db){
            $this->db = null;
        }
    }

    /**
     * Gets the only instance available
     */
    public function setInstance($dns, $username, $password){
        self::$db = new \PDO($dns, $username, $password);
    }

    public function __destruct(){
        self::$db = null;
    }
}
