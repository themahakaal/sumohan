<?php

/**
 * @author Sukhdev Mohan
 * @email sukhdev[dot]mohan[at]gmail[dot]com
 * @version 1.0
 */

namespace Sumohan\Config;

class Configuration {
    /**
     * @var string $db_driver the driver of the used database (MySQL, PostgreSQL etc).
     */
    private static $db_driver = '';

    /**
     * @var string $db_hostname the host of the given database.
     */
    private static $db_hostname = '';

    /**
     * @var string $db_name The name of the database.
     */
    private static $db_name = '';

    /**
     * @var string $db_username the username for the given database login.
     */
    private static $db_username = '';

    /**
     * @var string $db_password the password for the given database login.
     */
    private static $db_password = '';

    /**
     * This method will return the dsn for the PDO;
     *
     * @return string the PDO dsn;
     */
    public static function getDSN()
    {
        return self::$db_driver .':host='. self::$db_hostname .';dbname=' . self::$db_name ;
    }

    /**
     * This method will return the username for the Database login;
     *
     * @return string Username for the database login.
     */
    public static function getDbUsername()
    {
        return self::$db_username;
    }

    /**
     * This method will return the password for the Database login;
     *
     * @return string Password for the database login.
     */
    public static function getDbPassword()
    {
        return self::$db_password;
    }

}